# Saving a Document to the Local Computer

1. Open a new document.
1. Head up to the File tab and choose Options on the bottom left corner.
![alt text](images/1.png)
2. Choose Save As from the side menu.  Then click on Browse.
![alt text](images/2.png)
3. Then navigate to where you want to save the file and click save.
![alt text](images/3.png)
